FROM perl:5

WORKDIR /opt/app/
COPY Makefile.PL .
RUN cpanm --installdeps -n .

COPY ./Learn.pl .

WORKDIR /usr/src/learn/
CMD ["perl", "./Learn.pl"]
