package Learn;

use strict;
use warnings;
use Mojolicious::Lite;

our $VERSION = 1.0.0;

use Net::Prometheus;

my @todos;

my $client  = Net::Prometheus->new;
my $counter = $client->new_counter(
    name   => 'http_requests_total',
    help   => 'Number of received requests',
    labels => [qw( code method )]
);

# Access request information
get '/' => sub {
    my ($c) = @_;
    my $req = $c->req;

    $c->stash( todos => \@todos );
    $c->render('todos');
};

get '/todo/:id' => sub {
    my ($c) = @_;
    my $id = $c->param('id');

    $c->render( json => $todos[$id] );
};

post '/todo/' => sub {
    my ($c) = @_;

    push @todos, $c->req->json;

    $c->render( json => $todos[-1] );
};

put '/todo/:id' => sub {
    my ($c) = @_;
    my $id = $c->param('id');

    $todos[$id] = $c->req->json;

    $c->render( json => $todos[$id] );
};

get '/metrics' => sub {
    my ($c) = @_;
    $c->render( text => $client->render );
};

# Incremement prometheus request counter
app->hook(
    after_dispatch => sub {
        my ($c) = @_;
        $counter->inc( $c->res->code, $c->req->method );
    }
);

app->start;

__DATA__
@@ todos.html.ep
<!DOCTYPE html>
<html>
  <head><title>TODO App</title></head>
  <body>
  <div>
    <h2>Todos:</h2>
    <% if (! @$todos) {%> 
      No todos added yet ...
    <% } %> 
    <ul style="list-style-positions: outside">
      <% while (my ($i, $todo) = each @$todos) { %>
        <% if ($todo) {%>
        <li>
          <input id="<%= $i %>" type="checkbox" <%= %$todo{done} ? 'checked' : '' %> value=<%= %$todo{description} =%>></input>
          <label for="<%= $i %>"><%= %$todo{description} %> </label>
        <br>
        </li>
        <% } %> 
      <% } %> 
    <ul>

    <form id="new_todo" style="margin-top:10px">
      <label for="add_todo">Add a new Todo:</label>
      <input id="add_todo" placeholder="Mow the lawn"></input>
    </form>
  </div>
  <script>

  document.getElementById('new_todo').addEventListener('submit', function(event){
    event.preventDefault();
    fetch("http://localhost:8080/todo/", {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({"description": event.srcElement[0].value, "done": false}),
    })
    .then(response => response.json())
    .then(result => {
      console.log(result);
      // Poor man todos reloading
      window.location.assign('http://localhost:8080/app')
    });
  })

  function update_todo(event) {
    console.log(event);

    fetch("http://localhost:8080/todo/" + event.target.id, {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({"description": event.target.value, "done": event.target.checked}),
    })
    .then(response => response.json())
    .then(result => {
      console.log(result);
    });
  }

  let todosCheckboxes = document.querySelectorAll("input[type=checkbox]");
  todosCheckboxes.forEach(function(checkbox) {
    checkbox.addEventListener('change', update_todo);
  });

  </script>
  </body>
</html>
